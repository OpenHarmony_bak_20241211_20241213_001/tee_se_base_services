add_subdirectory(code)

add_library(se_base_services_so SHARED)
target_link_options(
    se_base_services_so
    PRIVATE
    ${SHARED_LINKER_FLAGS}
)

target_include_directories(se_base_services_so PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(se_base_services_so PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(se_base_services_so PRIVATE dyn_services_main_obj)
target_link_libraries(se_base_services_so PRIVATE se_base_services_fwk_obj)
target_link_libraries(se_base_services_so PRIVATE se_apdu_core_obj)
target_link_libraries(se_base_services_so PRIVATE logger_obj)
target_link_libraries(se_base_services_so PRIVATE parcel_obj)

# common
target_link_libraries(se_base_services_so PRIVATE se_modules_common_ipc_stub_obj)

# pin auth
target_link_libraries(se_base_services_so PRIVATE se_modules_pin_auth_ipc_stub_obj)
target_link_libraries(se_base_services_so PRIVATE dyn_services_pin_auth_obj)

# sec storage
target_link_libraries(se_base_services_so PRIVATE se_modules_sec_storage_ipc_stub_obj)
target_link_libraries(se_base_services_so PRIVATE dyn_services_sec_storage_obj)

set_target_properties(se_base_services_so PROPERTIES OUTPUT_NAME "combine")
hw_sign_ta(se_base_services se_base_services.so.sec)
