# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# enable asan in testing mode
if(ENABLE_COVERAGE OR ENABLE_FUZZ)
    set(COVERAGE_FLAG "--coverage")
endif()

set(CMAKE_CXX_FLAGS "-O0 -fno-omit-frame-pointer -fsanitize=address -fexceptions ${COVERAGE_FLAG}")
set(CMAKE_C_FLAGS "-O0 -fno-omit-frame-pointer -fsanitize=address -fexceptions ${COVERAGE_FLAG}")
set(CMAKE_LINKER_FLAG "-fno-omit-frame-pointer -fsanitize=address ${COVERAGE_FLAG}")

# disable opt to make sure asan enable
set(CC_OPTIMIZATION_LEVEL -O0 -g)

set(CC_OVERALL_FLAGS -pipe)
set(CC_WARNING_FLAGS
    -Wall
    -Werror
    -Wextra
    -Wdate-time
    -Wfloat-equal
    -Wshadow
    -Wstack-protector
    -Wstrict-prototypes
    -Wswitch-default
    -Wconversion
    -Wcast-qual
    -Wvla
    -Wunused
    -Wundef
    -Wno-return-stack-address
)

set(CC_LANNGUAGE_FLAGS -fsigned-char -std=c17)

set(CC_GENERATION_FLAGS
    -fno-common
    -fno-short-enums
    -fvisibility=default
    -funwind-tables
    -ffunction-sections
    -fdata-sections
)

set(CC_OPTIMIZATION_FLAGS -fno-strict-aliasing -fno-optimize-sibling-calls)
set(CC_SECURITY_FLAGS
    -fPIC
    -fstack-protector-strong
    --param
    ssp-buffer-size=4
)
set(CC_DEFINE_FLAGS -DENABLE_TESTING=1)

set(SE_BASE_SERVICES_DEFAULT_CC
    ${CC_OPTIMIZATION_LEVEL}
    ${CC_OVERALL_FLAGS}
    ${CC_WARNING_FLAGS}
    ${CC_LANNGUAGE_FLAGS}
    ${CC_GENERATION_FLAGS}
    ${CC_MACHINE_DEPENDENT_FLAGS}
    ${CC_OPTIMIZATION_FLAGS}
    ${CC_SECURITY_FLAGS}
    ${CC_DEFINE_FLAGS}
)

set(SE_BASE_SERVICES_DEFAULT_INC ${CMAKE_SOURCE_DIR}/test/unittest/mocks/inc)

function(hw_sign_ta)
    message(STATUS "fake building ta")
endfunction()

function(hw_sign_ta_product)
    message(STATUS "fake building ta")
endfunction()
