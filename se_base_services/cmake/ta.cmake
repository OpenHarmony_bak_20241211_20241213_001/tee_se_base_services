find_package(
    Python3
    COMPONENTS Interpreter
    REQUIRED
)

set(CMAKE_INSTALL_PREFIX ${CMAKE_SOURCE_DIR}/build/cmake)

include($ENV{TA_ROOT_DIR}/ta_common/cmake/options.cmake)
include($ENV{TA_ROOT_DIR}/ta_common/cmake/functions.cmake)

set(SE_BASE_SERVICES_DEFAULT_INC
    ${iTrustee_SDK_includes}
    ${ta_common_includes}
    ${hisi_includes}
)
set(SE_BASE_SERVICES_DEFAULT_CC ${CC_ALL_OPTIONS})

add_custom_target(${CMAKE_PROJECT_NAME}_ta ALL COMMENT "${CMAKE_PROJECT_NAME} Building all ta package ......")
