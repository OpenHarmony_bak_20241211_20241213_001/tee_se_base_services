/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_DYNAMIC_SRV_MOCK_H
#define UNIT_TEST_INC_TEE_DYNAMIC_SRV_MOCK_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <gmock/gmock.h>

#include "function_mocker.h"

#include "tee_dynamic_srv.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {

class TeeDynamicSrvMock : public FunctionMocker<TeeDynamicSrvMock> {
public:
    MOCK_METHOD(TEE_Result, tee_srv_get_uuid_by_sender, (uint32_t sender, TEE_UUID *uuid));
    MOCK_METHOD(void, tee_srv_unmap_from_task, (uint32_t vaAddr, uint32_t size));
    MOCK_METHOD(int, tee_srv_map_from_task, (uint32_t inTaskId, uint32_t vaAddr, uint32_t size, uint32_t *virtAddr));
    MOCK_METHOD(void, tee_srv_cs_server_loop,
        (const char *taskName, const struct srv_dispatch_t *dispatch, uint32_t dispatchSize,
            struct srv_thread_init_info *curThread));
};
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS
#endif // UNIT_TEST_INC_TEE_DYNAMIC_SRV_MOCK_H