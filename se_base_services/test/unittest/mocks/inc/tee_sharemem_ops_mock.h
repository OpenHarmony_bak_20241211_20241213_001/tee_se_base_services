/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_SHAREMEM_OPS_MOCK_H
#define UNIT_TEST_INC_TEE_SHAREMEM_OPS_MOCK_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <gmock/gmock.h>

#include "function_mocker.h"
#include "tee_sharemem_ops.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {

class TeeAllocSharememAux : public FunctionMocker<TeeAllocSharememAux> {
public:
    MOCK_METHOD(void *, tee_alloc_sharemem_aux, (const struct tee_uuid *uuid, uint32_t size));
};
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS

#endif // UNIT_TEST_INC_TEE_SHAREMEM_OPS_MOCK_H