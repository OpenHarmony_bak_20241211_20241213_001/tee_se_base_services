/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tee_crypto_api.h"

#include <new>

#include <securec.h>

extern "C" {
TEE_Result TEE_AllocateOperation(TEE_OperationHandle *operation, uint32_t algorithm, uint32_t mode, uint32_t maxKeySize)
{
    (void)operation;
    (void)algorithm;
    (void)mode;
    (void)maxKeySize;
    return 0;
}

void TEE_FreeOperation(TEE_OperationHandle operation)
{
    (void)operation;
}

TEE_Result TEE_SetOperationKey(TEE_OperationHandle operation, const TEE_ObjectHandle key)
{
    (void)operation;
    (void)key;
    return 0;
}

void TEE_CipherInit(TEE_OperationHandle operation, const uint8_t *iv, uint32_t len)
{
    (void)operation;
    (void)iv;
    (void)len;
}

TEE_Result TEE_CipherDoFinal(TEE_OperationHandle operation, const uint8_t *srcData, uint32_t srcLen, uint8_t *destData,
    uint32_t *destLen)
{
    (void)operation;
    (void)srcData;
    (void)srcLen;
    (void)destData;
    (void)destLen;
    return 0;
}

void TEE_GenerateRandom(void *randomBuffer, uint32_t randomBufferLen)
{
    (void)randomBuffer;
    (void)randomBufferLen;
    return;
}
}