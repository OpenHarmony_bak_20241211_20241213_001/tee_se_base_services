/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_PIN_AUTH_CORE_H
#define CODE_MODULES_INC_PIN_AUTH_CORE_H

#include "card_channel.h"
#include "pin_auth_ipc_defines.h"
#include "se_module_pin_auth_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode ProcPinAuthCmdEnrollLocal(const CardChannel *channel, uint32_t slotId, const PinDataHash *hash,
    PinDataSecret *secret);

ResultCode ProcPinAuthCmdAuthLocal(const CardChannel *channel, uint32_t slotId, const PinDataHash *hash,
    PinAuthResult *result);

ResultCode ProcPinAuthCmdEnrollRemote(const CardChannel *channel, uint32_t slotId, const PinDataRemoteBase *base);

ResultCode ProcPinAuthCmdAuthRemotePrepare(const CardChannel *channel, uint32_t slotId, uint64_t session,
    PinDataRemoteServiceChallenge *challenge);

ResultCode ProcPinAuthCmdAuthRemote(const CardChannel *channel, uint32_t slotId, uint64_t session,
    const PinDataRemoteClientProof *proof, PinAuthResult *result);

ResultCode ProcPinAuthCmdAuthRemoteAbort(const CardChannel *channel, uint32_t slotId, uint64_t session);

ResultCode ProcPinAuthCmdSetSelfDestructEnable(const CardChannel *channel, uint32_t slotId,
    const PinDestructConfig *config, const PinDataHash *hash, PinConfigResult *configResult);

ResultCode ProcPinAuthCmdGetNumSlots(const CardChannel *channel, uint32_t *numSlots);

ResultCode ProcPinAuthCmdGetFreezeStatus(const CardChannel *channel, uint32_t slotId, PinFreezeStatus *pinFreezeStatus);

ResultCode ProcPinAuthCmdSetFreezePolicy(const CardChannel *channel, uint16_t punishStartCnt, uint16_t destructMaxCnt,
    uint32_t enableDestructDefault);

ResultCode ProcPinAuthCmdGetFreezePolicy(const CardChannel *channel, uint16_t *punishStartCnt, uint16_t *destructMaxCnt,
    uint32_t *enableDestructDefault);

ResultCode ProcPinAuthCmdEraseSingleSlot(const CardChannel *channel, uint32_t slotId);

ResultCode ProcPinAuthCmdEraseAllSlot(const CardChannel *channel);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_PIN_AUTH_CORE_H