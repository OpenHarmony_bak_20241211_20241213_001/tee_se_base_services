/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERFACES_SE_BASE_MODULE_COMMON_IPC_DEFINES_INNER_H
#define INTERFACES_SE_BASE_MODULE_COMMON_IPC_DEFINES_INNER_H

#include "se_module_common_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct SeCommonSetServiceConfInput {
    ServiceId sid;
    uint32_t lock;
    SeServiceConfig config;
} SeCommonSetServiceConfInput;

#define MAX_INIT_KEY_LEN 256
typedef struct SeCommonSetBindingKeyInput {
    uint32_t initKeyLength;
    uint8_t initKey[MAX_INIT_KEY_LEN];
    uint32_t initKvn;
    ServiceId sid;
} SeCommonSetBindingKeyInput;
#ifdef __cplusplus
}
#endif

#endif // INTERFACES_SE_BASE_MODULE_COMMON_IPC_DEFINES_INNER_H