add_library(parcel_obj OBJECT src/parcel.c)

target_include_directories(parcel_obj PUBLIC inc)

target_include_directories(parcel_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(parcel_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(parcel_obj PRIVATE se_base_services_defines)

if(OPTION_SECUREC_INDEPENDENT)
    message(STATUS "parcel_obj use independent secure c.")
    target_link_libraries(parcel_obj PRIVATE securec_interface)
endif()
