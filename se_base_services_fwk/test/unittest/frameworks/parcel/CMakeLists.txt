include(GoogleTest)

add_executable(parcel_test src/parcel_test.cpp)

target_include_directories(parcel_test PRIVATE inc)
target_include_directories(parcel_test PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(parcel_test PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(parcel_test PRIVATE parcel_obj)
target_link_libraries(parcel_test PRIVATE securec)
target_link_libraries(parcel_test PRIVATE GTest::gtest_main)

gtest_discover_tests(parcel_test)
