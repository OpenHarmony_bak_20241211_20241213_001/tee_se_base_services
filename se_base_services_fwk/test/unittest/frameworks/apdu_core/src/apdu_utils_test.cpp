/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "apdu_utils.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
TEST(ApduUtilsTest, DataRevertUint16)
{
    uint16_t data = 0x1234;
    DataRevert((uint8_t *)&data, sizeof(data));
    EXPECT_EQ(data, 0x3412);
}

TEST(ApduUtilsTest, DataRevertUint32)
{
    uint32_t data = 0x12345678;
    DataRevert((uint8_t *)&data, sizeof(data));
    EXPECT_EQ(data, 0x78563412);
}

TEST(ApduUtilsTest, DataRevertUint64)
{
    uint64_t data = 0x12345678aabbccdd;
    DataRevert((uint8_t *)&data, sizeof(data));
    EXPECT_EQ(data, 0xddccbbaa78563412);
}

TEST(ApduUtilsTest, DataRevertInvalidInput)
{
    DataRevert(nullptr, 0);

    uint32_t data = 12345678;
    DataRevert((uint8_t *)&data, 0);
    EXPECT_EQ(data, 12345678);
    DataRevert((uint8_t *)&data, sizeof(uint32_t) - 1);
    EXPECT_EQ(data, 12345678);
}

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS