/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST_HELPERS_H
#define TEST_HELPERS_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <string>
#include <vector>

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
class TestHelpers {
public:
    static std::string VectorToHexString(const std::vector<uint8_t> &input);
    static std::string VectorToHexString(const uint8_t *input, uint32_t length);
    static std::vector<uint8_t> HexStringToVector(const std::string &input);
    static void VectorFillUp(const std::vector<uint8_t> &input, uint8_t *output, uint32_t *length);
    static void StringFillUp(const std::string &input, uint8_t *output, uint32_t *length);
};
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS

#endif // TEST_HELPERS_H
