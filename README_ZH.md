# 通用规范
[OpenHarmony安全芯片管理框架高安业务通用规范](DOCS/common_specifications/Readme_ZH.md)

# API规范
[OpenHarmony安全芯片管理框架高安业务API规范](DOCS/api_specifications/Readme_ZH.md)

# APDU规范
[OpenHarmony安全芯片管理框架高安业务APDU规范](DOCS/apdu_specifications/Readme_ZH.md)

# 测试规范
[OpenHarmony安全芯片管理框架高安业务测试规范](DOCS/test_specifications/Readme_ZH.md)
